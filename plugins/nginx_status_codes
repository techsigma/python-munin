#!/usr/bin/python3

import re, sys, os
from munin import MuninPlugin

class NginxStatusCodes(MuninPlugin):
    title = "nginx Error Codes"
    args = "--base 1000 -l 0"
    period = "minute"
    vlabel = "responses codes per minute"
    scale = False
    category = "Nginx"
    warning = os.environ.get('mobience_warn', 100)
    critical = os.environ.get('mobience_crit', 200)

    fields = (
    ('create', dict(
      label="/createUser",
      type="DERIVE",
      draw="LINE2",
      min="0",
      info="Non-200 response codes per minute",
      warning=str(warning),
      critical=str(critical)

    )),
    ('config', dict(
      label="/config",
      type="DERIVE",
      draw="LINE2",
      min="0",
      info="Non-200 response codes per minute",
      warning=str(warning),
      critical=str(critical)

    )),
    ('log', dict(
      label="/log",
      type="DERIVE",
      draw="LINE2",
      min="0",
      info="Non-200 response codes per minute",
      warning=str(warning),
      critical=str(critical)

    ))

    )

    def execute(self):
      # log_path = "/var/log/nginx/testapp.mobience.pl.access.log"
      log_path = "/var/log/nginx/app.mobience.pl.access.log"
      result = self.read_file(log_path=log_path)
      return dict(
        create=result["create"] if "create" in result else None,
        config=result["config"] if "config" in result else None,
        log=result["log"] if "log" in result else None
      )

    @staticmethod
    def read_file(log_path):
        result = {"create": 0, "config": 0, "log": 0}

        CODES = {
          '400': 'Bad Request',
          '401': 'Unauthorized',
          '403': 'Forbidden',
          '404': 'Not Found',
          '405': 'Method Not Allowed',
          '406': 'Not Acceptable',
          '408': 'Request Timeout',
          '499': 'Client Connection Terminated',
          '500': 'Internal Server Error',
          '502': 'Bad Gateway',
          '503': 'Service Unavailable',
          '504': 'Gateway timeout',
          'Other': 'Other responses'
        }

        results_code_createuser = 0 #{k: 0 for k in CODES}
        results_code_log = 0 #{k: 0 for k in CODES}
        results_code_config = 0 #{k: 0 for k in CODES}
        results_code_other = 0 #{k: 0 for k in CODES}
        if os.path.exists(log_path):
            with open(log_path, 'r') as f:
                for line in f.readlines():
                  code = re.search(" (\d\d\d) ", line)

                  if code:
                    code = code.group(0).strip()
                    if code in CODES:


                      if "/createuser" in line:
                        results_code_createuser += 1
                        # print("results_code_createuser[{}]==={}".format(code, results_code_createuser[code]))
                        result["create"] = results_code_createuser
                      if "/config" in line:
                        results_code_config += 1
                        # print("results_code_config[{}]==={}".format(code, results_code_config[code]))
                        result["config"] = results_code_config
                      if "/log" in line:
                        results_code_log += 1
                        # print("results_code_log[{}]==={}".format(code, results_code_log[code]))
                        result["log"] = results_code_log
                    elif int(code) >= 400:
                        results_code_other += 1

        else:
            return {}
        return result

if __name__ == "__main__":
  # NginxStatusCodes().config()
  NginxStatusCodes().run()