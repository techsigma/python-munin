
Munin Plugins for Mobience
============

Plugins
----------
* nginx_status_codes   : Non-200 response codes per minute

Requirements
-----------
* python3+

Installation (ubuntu)
------------

**Install Python-Munin**

* git clone https://bitbucket.org/techsigma/python-munin.git /tmp/python-munin
* cd /tmp/python-munin
* sudo python setup.py install

**Install plugins**

* sudo cp /tmp/python-munin/plugins/nginx_status_codes /usr/share/munin/plugins
* sudo ln -sf /usr/share/munin/plugins/nginx_status_codes /etc/munin/plugins/nginx_status_codes
* sudo chmod +x /usr/share/munin/plugins/nginx_status_codes
* sudo service munin-node restart
    
Check if plugins are running:

* munin-node-configure | grep "nginx_status_codes"

Test plugin output:

* munin-run nginx_status_codes


NOTE:
On Server4 change the code in file:
 https://bitbucket.org/techsigma/python-munin/src/555a40a9cd0ec62d0a4793ca2a8e7480a02dc9c1/plugins/nginx_status_codes?at=master&fileviewer=file-view-default
 at Line 51 from:
    log_path = "/var/log/nginx/app.mobience.pl.access.log"
 to:
    log_path = "/var/log/nginx/testapp.mobience.pl.access.log"
